#Grunt-setup-files V1.2


Running PHP server on existing project with grunt

**Tested on**
- Ubuntu 14.04 LTS VM 

**Contains:**
- dev folder with simple dummy project
- deploy folder
- Gruntfile.js
- package.json

[1] **Requirements:**
- grunt

From manual http://gruntjs.com/getting-started

>Getting started

>Grunt and Grunt plugins are installed and managed via npm, the Node.js package manager. Grunt 0.4.x requires stable Node.js versions >= 0.8.0. Odd version numbers of Node.js are considered unstable development versions.

>Before setting up Grunt ensure that your npm is up-to-date by running npm update -g npm (this might require sudo on certain systems).

>If you already have installed Grunt and are now searching for some quick reference, please checkout our Gruntfile example and how to configure a task.

>Installing the CLI

>Using Grunt 0.3? Please see Grunt 0.3 Notes

>In order to get started, you'll want to install Grunt's command line interface (CLI) globally. You may need to use sudo (for OSX, *nix, BSD etc) or run your command shell as Administrator (for Windows) to do this.

`npm install -g grunt-cli`

[2] **Install:**
- Check if port is available (default port used is 3001)
- cd into project directory
- run command `sudo npm install`
- this will install these packages:
  - "devDependencies": {
    - "grunt": "~0.4.5",
    - "grunt-autoprefixer": "~3.0.4",
    - "grunt-sass": "~1.1.0",
    - "grunt-browser-sync": "~2.2.0",
    - "grunt-contrib-watch": "~1.0.0",
    - "grunt-contrib-cssmin": "~1.0.1",
    - "grunt-contrib-uglify": "~1.0.1",
    - "grunt-contrib-htmlmin": "~1.0.0",
    - "grunt-contrib-sass": "~1.0.0",
    - "grunt-php": "~1.5.1"}

[3] **Usage:**
- In project directory run `grunt` command
- This will start (finger crossed) PHP 5.4.0+ server (more info @ https://www.npmjs.com/package/grunt-php ) on `localhost:3001` and open default browser
- To test if PHP is running you can go to `localhost:3001/php/test.php` or look at CLI
- When you save any file it will run predefined tasks in Gruntfile.js

  e.g. On save scss it will run `scss`->`autoprefixer`->`cssmin` and create minified .css file in deploy folder
